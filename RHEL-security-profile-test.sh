#!/bin/bash

# Script performs TLS 1.3 and TLS 1.1 check with DEFAULT and LEGACY security policies in RHEL 8 operating system

function setup(){
  initial_policy=$(update-crypto-policies --show)
  openssl req -x509 -newkey rsa -keyout key.pem -out server.pem -days 365 -nodes -subj "/CN=localhost" 2>/dev/null
}

function cleanup(){
  rm key.pem server.pem # delete the generated files 
  echo "########################################"
  echo "Re-applying the original profile $initial_policy"
  sudo update-crypto-policies --set $initial_policy >/dev/null

}

function test_tls13(){
  openssl s_server -4 -key key.pem -cert server.pem -quiet   < /dev/stdin  > /dev/null 2>&1 & # didn't quite get why need to use quiet mode and why  "</dev/stdin" is required
  pid=$!
  openssl s_client -connect localhost -tls1_3 > /dev/null 2>&1 <<< Q # testing TLS 1.3, Q is required  for terminating the process
  if  [ $? -eq 0 ]
    then echo "TLS1.3 - PASS"
    else echo "TLS1.3 - FAIL" 
  fi
  kill $pid && wait $pid 2>/dev/null
}


function test_tls11(){
  openssl s_server -4 -key key.pem -cert server.pem -quiet   < /dev/stdin  > /dev/null 2>&1 & # didn't quite get why need to use quiet mode and why  "</dev/stdin" is required
  pid=$!
  openssl s_client -connect localhost -tls1_1 > /dev/null 2>&1 <<< Q # testing TLS 1.1
  if  [ $? -eq 0 ]
    then echo "TLS1.1 - PASS"
    else echo "TLS1.1 - FAIL" 
  fi
  kill $pid && wait $pid 2>/dev/null # kill server
}



# testing the LEGACY profile behavior
function test_legacy(){
  echo "##############################"
  echo "Enabling and testing LEGACY policy"
  sudo update-crypto-policies --set LEGACY >/dev/null
  test_tls13
  test_tls11
}

# testing the DEFAULT profile behavior
function test_default(){
  echo "##############################"
  echo "Enabling and testing DEFAULT policy"
  sudo update-crypto-policies --set DEFAULT >/dev/null
  test_tls13
  test_tls11
}

setup # run setup before anything else

if [[ $# -eq 1 ]]
then
  case $1 in
    -l) # list available tests
      echo "Available tests:"
      echo "test-default"
      echo "test-legacy"
      exit 0
      ;;
    --test-default) # run only test_default
      test_default
      ;;
    --test-legacy) # run only test_legacy
      test_legacy
      ;;
    *) # show some help when invalid arguments are provided
      echo "Wrong argument"
      echo "Run script without any arguments to run all tests"
      echo "Otherwise specify the name of the test to run, for example 'test.sh --test-legacy'"
      echo "To list all available tests run 'test.sh -l'"
      exit 1
      ;;
  esac
fi

if [[ $# -eq 0 ]] # if no args are provided then run all the tests
then
  test_default
  test_legacy
fi

cleanup
